import java.io.*;
import java.util.*;

public class Lonelyinteger {
	static int lonelyinteger(int[] a) {
		int b[]=new int[101];
		int size=a.length;
		
		for(int i=0;i<size;i++)
			b[a[i]]++;
		
		int index=a[0];
		for(int i=0;i<101;i++) {
			if(b[i]%2==1) {
				index=i;
				break;
			
			}
		}
		
		return index;       
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[] a = new int[n];
        for(int a_i = 0; a_i < n; a_i++){
            a[a_i] = in.nextInt();
        }
        int result = lonelyinteger(a);
        System.out.println(result);
    }
}

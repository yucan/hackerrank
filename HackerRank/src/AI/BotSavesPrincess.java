package AI;

import java.util.Scanner;

public class BotSavesPrincess {

	
	public static void savePrincess(int n, char [][]board) 
	{
		int mx = 0,my=0, px=0,py=0;
		
		//find coordinates of princess and 
		for(int i=0;i<n;i++) 
		{
			for(int j=0;j<n;j++) 
			{
				if(board[i][j]=='m') 
				{
					mx=i; my=j;
				}
				if(board[i][j]=='p') 
				{
					px=i; py=j;
				}
			}
		}
		
		int rowDiff=mx-px;
		int colDiff=my-py;
		
		String rowDirection=(rowDiff<0)?"DOWN":"UP";
		String colDirection=(colDiff<0)?"RIGHT":"LEFT";
		
		for(int i=0; i<Math.abs(rowDiff);i++) 
			System.out.println(rowDirection);
		
		for(int i=0; i<Math.abs(colDiff);i++) 
			System.out.println(colDirection);		
		
	}
	
	public static void main(String[] args) {
		 Scanner in = new Scanner(System.in);
		  
		  //if(in.hasNextLine())
		  
	      int n =Integer.parseInt(in.nextLine());  //size of matrix
	      char [][]board = new char[n][n];
	      
	      
	      for(int i=0;i<n;i++) {
	    	  board[i]=in.nextLine().toCharArray();
	    	  //board[i]=new char[c];
	      }
	      in.close();
	      
	      savePrincess(n,board);

	}

}

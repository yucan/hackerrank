package days.statistics;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.TreeMap;

public class MeanMedianMode {
 
	
	public static void mean(int []arr , int size) 
	{
		long sum=0;
		for(int i=0;i<size;i++)
			sum+=arr[i];
		
		double mean=1.0*sum/size;
		System.out.println(mean);
		//return mean;
	}
	public static void median(int []arr , int size) 
	{
		Arrays.sort(arr);
		double median =0 ;
		
		if(size%2==0)
			median = ( arr[size/2-1] + arr[size/2]  ) /2.0 ;
		else 
			median = arr[size/2];
		
		System.out.println(median);
		
	}
	public static void mode(int []numArray , int size) 
	{
		Map<Integer, Integer> map = new HashMap<>();
		
		Arrays.sort(numArray);
		
		for(int i=0; i<size; i++)  {
			
			int elm = numArray[i];
					
		    if(!map.containsKey(elm))  
		    	map.put(elm, 1);
		    else {
		    	int val = map.get(elm);
		    	map.put(elm, val+1);
		    }		    
	  	}
		
		Map<Integer, Integer> treeMap = new TreeMap<>(map);  //sorts map according to keys
		
		for(int key : treeMap.keySet() )
			System.out.println(key + ":" + treeMap.get(key));
	}
	
	public static int mode2(int []numArray , int size) 
	{
		Map<Integer, Integer> map = new HashMap<>();

		for(int i=0; i<size; i++)  {
			
			int elm = numArray[i];
					
		    if(! map.containsKey(elm))  
		    	map.put(elm, 1);
		    else {
		    	int val = map.get(elm);
		    	map.put(elm, val+1);
		    }		    
	  	}
		
		int i=0;	
		int key1,val1;		
		int maxVal=0, key=0;
		
		for( Entry<Integer, Integer> entry : map.entrySet()) 		
		{			
			if(i==0)  
			{
				key=entry.getKey();
				maxVal=entry.getValue();
			}			
			else {
				
				key1 = entry.getKey();
				val1=entry.getValue();
				
				if(val1>maxVal) {
					maxVal=val1;
					key=key1;
				}
				else if (val1==maxVal) {
					if(key1<key)  key=key1;
				}								
			
			}
			i++;						
		}
		System.out.println(key);
		return key;
		

	}
	
	
	private static final Scanner scanner = new Scanner(System.in);
	
	public static void main(String[] args) {				
		
		int arCount = Integer.parseInt(scanner.nextLine().trim());

        int[] ar = new int[arCount];

        String[] arItems = scanner.nextLine().split(" ");

        for (int arItr = 0; arItr < arCount; arItr++) {
            int arItem = Integer.parseInt(arItems[arItr].trim());
            ar[arItr] = arItem;
        }
		
		
		//int ar [] = {64630,11735, 14216, 99233, 14470, 4978, 73429, 38120, 4978, 67060};
		//int ar2 [] = {64630,11735, 14216, 99233, 14470, 4978, 73429, 38120, 51135, 67060};
                    		//64630 11735 14216 99233 14470 4978 73429 38120 51135 67060

		//int size = 10;
		
		mean(ar,arCount);
		median(ar,arCount);		
		mode2(ar,arCount);					
		
	}

}

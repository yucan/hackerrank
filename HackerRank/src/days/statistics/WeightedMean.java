package days.statistics;

import java.util.Scanner;

public class WeightedMean {

	public static void calculate(int []arr , int [] weightArr , int size) 
	{
		double sum=0;
		double sumOfWeights=0;
		
		for(int i=0;i<size;i++) {
			sum+=arr[i]*weightArr[i];
		    sumOfWeights+=weightArr[i];	
		}
		
	    double weightedMean = sum*1.0/sumOfWeights;
		System.out.printf("%.1f",weightedMean);
			
		
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scanner = new Scanner(System.in);
		
		int size = Integer.parseInt(scanner.nextLine().trim());

        int[] ar = new int[size];

        String[] arItems = scanner.nextLine().split(" ");

        for (int arItr = 0; arItr < size; arItr++) {
            int arItem = Integer.parseInt(arItems[arItr].trim());
            ar[arItr] = arItem;
        }
        
        int[] weightArr = new int[size];

        String[] weightArrStr = scanner.nextLine().split(" ");

        for (int arItr = 0; arItr < size; arItr++) {
            int weightItem = Integer.parseInt(weightArrStr[arItr].trim());
            weightArr[arItr] = weightItem;
        }
        
        calculate(ar,weightArr,size);
        

	}

}

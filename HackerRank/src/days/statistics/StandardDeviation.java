package days.statistics;

import java.util.Scanner;

public class StandardDeviation {

	public static double mean(int []arr , int size) 
	{
		long sum=0;
		for(int i=0;i<size;i++)
			sum+=arr[i];
		
		double mean=1.0*sum/size;
		//System.out.println(mean);
		return mean;
	}
	public static double standartDeviation(int []arr, int size, double mean) 
	{
		double distanceSum = 0.0 ;
		
		for(int i=0;i<size;i++) 		
		{
			distanceSum+=Math.pow(arr[i]-mean,2);
		}
		double stdDeviation=Math.sqrt(distanceSum/size);
		return stdDeviation;
		
	}
	
	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		
		int size = Integer.parseInt(scanner.nextLine().trim());

        int[] arr = new int[size];

        String[] arItems = scanner.nextLine().split(" ");

        for (int i = 0; i < size; i++) {
            int item = Integer.parseInt(arItems[i].trim());
            arr[i] = item;
        }
        
        double meanRes = mean(arr, size);
        
        double stdDev = standartDeviation(arr, size, meanRes);
        
        System.out.printf("%.1f",stdDev);
        
	}

}

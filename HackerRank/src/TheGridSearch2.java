import java.io.*;
import java.util.*;

public class TheGridSearch2 {
	
	static String gridSearch(String[] G, String[] P) {
							
		boolean result=true;	
			
		String pattern=P[0];
		
		for(int n=0;n<1000;n++) {			
										
			for (int i=0;i<G.length ;i++) 
			{
				result=false;
				
				int firstIndex=G[i].indexOf(pattern,n);						
				
				if(firstIndex!=-1) {
					result=true;
					
					for(int j=1; j<P.length && i+j<G.length; j++) {
						int nextIndex=G[i+j].indexOf(P[j],n);
						if(nextIndex==-1 || nextIndex!=firstIndex) 
						{
							result=false;
							break;
						}
					}
				}
				if(result==true) break;
			}
			
			if(result==true) break;
			
		}
		
		return (result==true)?"YES":"NO";
        // Complete this function
    }

    public static void main(String[] args) {
    	
        Scanner in = new Scanner(System.in);
        int t = in.nextInt();
        for(int a0 = 0; a0 < t; a0++){
            int R = in.nextInt();
            int C = in.nextInt();
            String[] G = new String[R];
            for(int G_i = 0; G_i < R; G_i++){
                G[G_i] = in.next();
            }
            int r = in.nextInt();
            int c = in.nextInt();
            String[] P = new String[r];
            for(int P_i = 0; P_i < r; P_i++){
                P[P_i] = in.next();
            }
            String result = gridSearch(G, P);
            System.out.println(result);
        }
        in.close();
        
    	//gridSearch(null,null);
    }

}

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class PlusMines {

    static void plusMinus(int[] arr) {
        
    	int size = arr.length;
    	
    	int plus=0,mines=0,zero=0;
   	
    	
    	for (int i = 0; i < size; i++) {
			if(arr[i]>0) plus++;
			else if(arr[i]<0) mines++;
			else zero++;
		}
    	double plusRatio=0, minesRatio=0, zeroRatio=0;
    	
    	if (plus>0) plusRatio=plus*1.0/size;
    	if (mines>0) minesRatio=mines*1.0/size;
    	if (zero>0) zeroRatio=zero*1.0/size;
    	
    	DecimalFormat df = new DecimalFormat("#.######");
    	df.setRoundingMode(RoundingMode.CEILING);
    	for (Number n : Arrays.asList(plusRatio,minesRatio,zeroRatio)) {
    	    Double d = n.doubleValue();
    	    System.out.println(df.format(d));
    	}    	    	
    	
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[] arr = new int[n];
        for(int arr_i = 0; arr_i < n; arr_i++){
            arr[arr_i] = in.nextInt();
        }
        plusMinus(arr);
        in.close();
    }
}
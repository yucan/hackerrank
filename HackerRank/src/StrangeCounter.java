import java.io.*;
import java.util.*;

public class StrangeCounter {
	static long strangeCode(long t) {
		
		//block indexini bulma
		
		int blockIndex=0;
		
		
		if(t-1==0) 
			blockIndex=0;
		else 
			blockIndex = (int) (Math.floor(Math.log(t-1)/Math.log(3)));	
		//first value index
		int blockFirstIndex = (int) (1+(3*(Math.pow(2, blockIndex)-1)));
		
		//first value of block			
		int blockFirstVal = (int) (3*Math.pow(2,blockIndex));
				
		long result = blockFirstIndex+blockFirstVal-t;
			
		return result;        
    }

    public static void main(String[] args) {
       /* Scanner in = new Scanner(System.in);
        long t = in.nextLong();
        long result = strangeCode(t);
        System.out.println(result);
        in.close();
        */
    	long result = strangeCode(22);
        System.out.println(result);
    	
    }
}

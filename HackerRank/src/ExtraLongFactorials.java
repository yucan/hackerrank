import java.io.*;
import java.math.BigInteger;
import java.util.*;

public class ExtraLongFactorials {
	static void extraLongFactorials(int n) {
        BigInteger b=new BigInteger("1");
        
        for(int i=2;i<=n;i++)
        	b=b.multiply(BigInteger.valueOf(i));
        
        System.out.println(b);
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        extraLongFactorials(n);
        in.close();
    }
}

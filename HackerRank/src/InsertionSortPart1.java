import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class InsertionSortPart1 {
	static void insertionSort1(int n, int[] arr) {
	        // Complete this function
		
		int size = arr.length;
		int val = arr[size-1]; //last value
		
		boolean status=false;
		for(int i=size-1-1;i>=0 && status==false; i--) 
		{
			if(arr[i]>val) arr[i+1]=arr[i];
			else {
				arr[i+1]=val; 
				status=true;
			}
			for(int j=0;j<size;j++)
				System.out.print(arr[j]+" ");
			System.out.println("");
		}
		if(status==false) {
			arr[0]=val;		
			for(int j=0;j<size;j++)
				System.out.print(arr[j]+" ");
		}
	}

	public static void main(String[] args) {
	        Scanner in = new Scanner(System.in);
	        int n = in.nextInt();
	        int[] arr = new int[n];
	        for(int arr_i = 0; arr_i < n; arr_i++){
	            arr[arr_i] = in.nextInt();
	        }
	        insertionSort1(n, arr);
	        in.close();
	}
}

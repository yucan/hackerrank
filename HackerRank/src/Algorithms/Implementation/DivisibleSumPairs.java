package Algorithms.Implementation;

import java.util.Scanner;

public class DivisibleSumPairs {
	
	private static final Scanner scanner = new Scanner(System.in);
	
	static int divisibleSumPairs(int n, int k, int[] ar) {

		System.out.println("asdfffff");
				
		int counter=0;
		
		for(int i=0; i<n;i++)
			for(int j=i+1;j<n;j++)
				if((ar[i]+ar[j])%k==0) {
					System.out.println(ar[i]+"+"+ar[j] +" = "+ ar[i]+ar[j]);
					counter++; 
				}
				
		return counter;
				
	}

	public static void main(String[] args) {
		  

	        String[] nk = scanner.nextLine().split(" ");

	        int n = Integer.parseInt(nk[0]);

	        int k = Integer.parseInt(nk[1]);

	        int[] ar = new int[n];

	        String[] arItems = scanner.nextLine().split(" ");
	        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

	        for (int i = 0; i < n; i++) {
	            int arItem = Integer.parseInt(arItems[i]);
	            ar[i] = arItem;
	        }

	        int result = divisibleSumPairs(n, k, ar);

	        System.out.println(result);

	        scanner.close();
	    
	}

}

package InterviewPreparationKit;

public class WarmUpChallenges {
	
    // Complete the sockMerchant function below.
    static int sockMerchant(int n, int[] ar) {

    	int []res = new int[101];
    	for(int i=0;i<n;i++) 
    		res[ar[i]]++;
    	
    	int numOfPairs=0;
    	
    	for(int i=0;i<101;i++)
    		numOfPairs+=res[i]/2;
    	return numOfPairs;
    }
    public static void testSockMerchant()
    {
    	int []ar = { 10, 20, 20, 10, 10, 30, 50, 10, 20};
		int n=9;
		System.out.println(sockMerchant(n,ar));	
    } 
    static int countingValleys(int n, String s) {

    	int numOfValleys=0;
    	int level=0;
    	
    	for(int i=0; i<n; i++ ) 
    	{
    		if(s.charAt(i)=='U') {
    			level++;
    			if(level==0) numOfValleys++;
    			
    		}
    		else 
    		{ 
    			level--;
    		}
    		
    	}
    	return numOfValleys;
    }
    public static void testCountingValleys()
    {
    	String s="UDDDUDUU";
		int n=8;
		System.out.println(countingValleys(n,s));	
    } 
    
    public static int jumpingOnClouds(int[] c) 
    {
    	int n = c.length;
    	
    	int numOfJumps=0;
    	
    	for(int i=0; i<n-1; ) 
    	{
    		
    		if(i+2<n && c[i+2]!=1) {
    			i=i+2;
    			
    		}
    		else
    			i=i+1;
    		numOfJumps++;
    		
    		//System.out.print(i+" ");
    	}
    	//System.out.println("");
    	return numOfJumps;
    			
    }
    public static void testjumpingOnClouds()
    {
    	//int []c = {0, 0, 1, 0, 0, 1, 0};
    	int []c= {0, 0, 0, 0, 1, 0};
		
		System.out.println(jumpingOnClouds(c));	
    } 
    
    static long repeatedString(String s, long n) 
    {
    	int numOfaS=0;
    	for(int i=0; i< s.length();i++)
    		if(s.charAt(i)=='a') numOfaS++;
    	
    	long divident = n/s.length();
    	
    	
    	long remainder= n-s.length()*divident;
    	
    	long res=0;
    	res+=numOfaS*divident;
    	
    	for(int i=0; i< remainder;i++)
    		if(s.charAt(i)=='a') res++;
    	
    	return res;
    }

    public static void testRepeatedString()
    {
    	String s="aba";
    	long n = 10;
    	
		
		System.out.println(repeatedString(s,n));	
    } 
    
	public static void main(String[] args) {
		
		//testCountingValleys();
		//testjumpingOnClouds();
		testRepeatedString();
	

	}

}

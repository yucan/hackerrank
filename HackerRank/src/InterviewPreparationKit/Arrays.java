package InterviewPreparationKit;

public class Arrays {
	
	
	static long arrayManipulation(int n, int[][] queries) {
	//baz� test case ler i�in timeout olu�uyor.	
		
		long numQueries=queries.length;
		
		long []arr=new long[n];
		
		int minIdx = queries[0][0];
		int maxIdx = queries[0][1];
		
		for(int i=0; i<numQueries ; i++) 
		{
		
			
			int a=queries[i][0];
			int b=queries[i][1];
			int k=queries[i][2];
			
			for(int j=a-1; j<b; j++)
				arr[j]+=k;
			//System.out.println();
			
			if(a<minIdx) minIdx=a;
			if(b>maxIdx) maxIdx=b;
				
		}
		long max=arr[0];
		
		for(int i=minIdx;i<maxIdx;i++) 
		{
			if(arr[i]>max) max=arr[i];
		}
		return max;
	}
	static void testArrayManipulation() 
	{		
		int [][]a = {{1,2,3},{4,5,6},{4,5,6}};
        int n=10; 
        int[][] queries= {{2, 6, 8},{3, 5, 7},{1, 8, 1},{5, 9, 15}};
	    System.out.println(queries.length);
	    System.out.println(arrayManipulation(n,queries));
	}
    static int[] rotateLeft(int[] a, int d) {
    	int size=a.length;
    	
    	int []res = new int[size];
    	
    	for(int i=d;i<size;i++)
    		res[i-d]=a[i];
    	
    	for(int i=0; i<d; i++)
    		res[size-d+i]=a[i];
    		
    	return res;
    }
    static void testRotateLeft() 
	{
    	int []a= {1, 2, 3, 4, 5};
    	int d=4;
    	    	
    	for (int i : rotateLeft(a, d)) {
       		System.out.println(i + " ");			
		}     	
    	
	}
    static int hourglassSum(int[][] arr) 
    {
    	int []elems  = new int[16];    	    
    	
    	for(int i=1;i<5;i++) 
    	{
    		for (int j = 1; j < 5; j++) {
    			    			
    			elems[(i-1)*4+j-1]=arr[i-1][j-1]+arr[i-1][j]+arr[i-1][j+1] +
    					arr[i][j]+
    				arr[i+1][j-1]+arr[i+1][j]+arr[i+1][j+1];
    			
			}
    	}
    	
    	int max=elems[0];
    	
    	for (int i = 0; i < elems.length; i++) {
			if(max<elems[i]) max=elems[i];
		}
    	return max;
    }
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		testRotateLeft();
	}

}

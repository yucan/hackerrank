package InterviewPreparationKit;

public class Miscellaneous {

	static long flippingBits(long n) {
        //unsigned long max=4294967295;
        return 4294967295l^n;

    }

	static int[] maxXor(int[] arr, int[] queries) {
        // solve here
    	int arrSize = arr.length;
    	int queriesNumber = queries.length;
    	
    	int []result= new int[queriesNumber];
    	
    	for(int i=0;i<queriesNumber;i++) {
    		
    		int max=arr[0]^queries[i];
    		
	    	for(int j=1;j<arrSize;j++) 
	    	{
	    		int temp=arr[j]^queries[i];
	    		if(temp>max) max=temp;
	    		
	    	}
	    	result[i]=max;
	    	//System.out.println(max);
    	}
    	return result;

    }
    public static void testMaxXor() 
    {
    	int [] arr= {0, 1, 2};
    	int [] queries= {3,7, 2};    	
    	int []result = maxXor(arr, queries);
    	for(int i=0;i<result.length;i++)
    		System.out.println(result[i]);
    	
    	int [] arr1= {5, 1, 7, 4, 3};
    	int [] queries1= { 2, 0};    	
    	int []result1 = maxXor(arr1, queries1);
    	
    	for(int i=0;i<result1.length;i++)
    		System.out.println(result1[i]);
    }
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//System.out.println(flippingBits(9));
		testMaxXor();
		//System.out.println(2^2);
		

	}

}

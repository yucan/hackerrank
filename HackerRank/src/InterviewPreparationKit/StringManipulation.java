package InterviewPreparationKit;

import java.util.Arrays;

public class StringManipulation {
	
	static String isValid(String s) 
	{
		//calismadi - 30.04.2019
		int []frequencies= new int [26]; //a=97, z=122 , z-a+1=122-97+1=26 
		

		for (int i = 0; i < s.length(); i++) {
			frequencies[s.charAt(i)-97]++;
		}
		/*
		for (int i = 0; i < frequencies.length; i++) {
			
			if(frequencies[i]!=0) {
				int ascii=i+97;
				char ch=(char)ascii;
				System.out.println(ch+" : " + frequencies[i]);
			}
		}*/
		
		int numOfDiff=0;
		
		String result="YES";
		
		Arrays.sort(frequencies);
		for (int i = 0; i < frequencies.length-1; i++) {
			
			if(frequencies[i]==0) continue;   // eliminate 0 occurrences
			
			int diff = Math.abs(frequencies[i]-frequencies[i+1]);
			
			if(diff==1) 
			{ 
				numOfDiff++;
				if(numOfDiff>1) 
				{
					result="NO";
					break;
				}
			}
			else if(diff>1) 
			{
				result="NO";
				break;
			}
		}
		
		return result;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(isValid("aabbcd"));

	}

}

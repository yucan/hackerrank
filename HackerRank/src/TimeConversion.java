import java.io.*;

//Çözülemedi
public class TimeConversion {

	 static String timeConversion(String s) {
		 /*
		     12:00 AM : midnight
		     12:00 PM : Noon
		     AM : Before Noon
		     PM : After Noon		     
		 */
		 
		 String time24=s.substring(0, s.length()-2);		 
		 
		 String type = s.substring(s.length()-2, s.length());
		 
		 String []tokens = time24.split(":");
		 
		 int hour = Integer.parseInt(s.split(":")[0]);		 
		 
		 int hour2 = hour;
		 String strhour2="";
		 
		 if(type.equals("PM") || (type.equals("AM") && hour==12)) {
			 
			 hour2=(hour+12)%24;
			 strhour2=hour2+"";
			 
			 if(hour2<10) strhour2="0"+strhour2;
			 tokens[0]=strhour2;
		 }		 				
		
		 String newHour = tokens[0]+":"+tokens[1]+":"+tokens[2];
		 		 
		 return newHour.trim();
	       
	 }

	 public static void main(String[] args) {
	       /* Scanner in = new Scanner(System.in);
	        String s = in.next();
	        String result = timeConversion(s);
	        System.out.println(result);
	        */
		 String result = timeConversion("12:45:54PM");
		 System.out.println(result);
	 }
	    
}

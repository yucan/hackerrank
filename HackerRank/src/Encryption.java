import java.io.*;
import java.util.*;
import java.math.*;

public class Encryption {
	static String encryption(String s) {
		
		int size=s.length();
		double sqr=Math.sqrt(size);
		
		int numCols=(int) Math.ceil(sqr);
		int numRows=(int) Math.floor(sqr);
		
		if(numCols*numRows<size) 
		{					
			numRows+=1;			
		}
		
		String arr[][]=new String[numRows][numCols]; 
		
		for(int i=0;i<numRows;i++) 
		{
			for(int j=0;j<numCols && i*numCols+j<size ;j++) {
				
				arr[i][j]=s.charAt(i*numCols+j)+"";			
			}
		}
		
		String result = "";
		
		for(int i=0;i<numCols;i++) {
			String str="";
			for(int j=0; j<numRows; j++) 
			{
				if(arr[j][i]!=null)
					str+=arr[j][i];
			} 
			str+=" ";
			result+=str;
		}
		result=result.trim(); //last space character
		return result;
        
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String s = in.next();
        String result = encryption(s);
        System.out.println(result);
        in.close();
    }
}

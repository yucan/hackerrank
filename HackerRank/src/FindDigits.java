import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class FindDigits {
	
	static int findDigits(int n) {
		String s= n +"";
		int counter=0;
		for(int i=0;i<s.length();i++) 
		{
			int num=s.charAt(i);
			num=num-48;
			
			if(num==0) continue;
			
			if(n%num==0)
				counter++;
		}
		return counter;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int t = in.nextInt();
        for(int a0 = 0; a0 < t; a0++){
            int n = in.nextInt();
            int result = findDigits(n);
            System.out.println(result);
        }
        in.close();
    }
}

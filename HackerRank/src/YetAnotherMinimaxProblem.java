import java.io.*;
import java.util.*;
//Çözülemedi
public class YetAnotherMinimaxProblem {
	  static int anotherMinimaxProblem(int[] a) {
		  
		  int n0,n1;
		  int size=a.length;
		  int max=a[0]^a[1];
		  
		  int temp=max;
		  for(int i=1;i<size-1;i++) 
		  {
			  temp=a[i]^a[i+1];
			  if(temp>max)
				  max=temp;
		  }
		  
		return max;
	        // Complete this function
	    }

	    public static void main(String[] args) {
	        Scanner in = new Scanner(System.in);
	        int n = in.nextInt();
	        int[] a = new int[n];
	        for(int a_i = 0; a_i < n; a_i++){
	            a[a_i] = in.nextInt();
	        }
	        int result = anotherMinimaxProblem(a);
	        System.out.println(result);
	        in.close();
	    }
}
